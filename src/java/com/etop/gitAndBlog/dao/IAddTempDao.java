/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etop.gitAndBlog.dao;

import com.etop.gitAndBlog.common.Pager;
import com.etop.gitAndBlog.model.AddTemp;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface IAddTempDao {
	public void add(AddTemp addTemp);
	public void deleteAll();
	public AddTemp load(int id);
	public List<AddTemp> list();
        public List<AddTemp> list(Pager page);
        public int count();
}
