/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etop.gitAndBlog.dao;

import com.etop.gitAndBlog.model.Message;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface IMessageDao {
	public void add(Message message);
	public void delete(int id);
	public void update(Message message);
	public Message load(int id);
	public Message loadByName(String name);
	public List<Message> list();
	public List<Message> listByNumber(String number);    
}
