/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etop.gitAndBlog.dao;

import com.etop.gitAndBlog.common.Pager;
import com.etop.gitAndBlog.model.AddTemp;
import com.etop.gitAndBlog.model.GBException;
import com.etop.gitAndBlog.util.DBUtil;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Administrator
 */
public class AddTempDao implements IAddTempDao {

    private Connection con = null;
    private PreparedStatement ps = null;
    private ResultSet rs = null;

    @Override
    public void add(AddTemp addTemp) {
        try {
            con = DBUtil.getConnection();
            String sql = "insert into add_temp values(null,?,?,?,?,now());";
            ps = con.prepareStatement(sql);
            ps.setString(1, addTemp.getName());
            ps.setString(2, addTemp.getNumber());
            ps.setString(3, addTemp.getGit());
            ps.setString(4, addTemp.getBlog());
            ps.executeUpdate();
            DBUtil.extracted(rs, ps, con);
        } catch (SQLException ex) {
            Logger.getLogger(AddTempDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void deleteAll() {
        try {
            con = DBUtil.getConnection();
            String sql = "delete from add_temp;";
            ps = con.prepareStatement(sql);
            ps.executeUpdate();
            DBUtil.extracted(rs, ps, con);
        } catch (SQLException ex) {
            Logger.getLogger(AddTempDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public AddTemp load(int id) {
        try {
            con = DBUtil.getConnection();
            String sql = "select * from add_temp where id=?;";
            ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            if (rs.next()) {
                AddTemp addTemp = new AddTemp(rs.getInt("id"), rs.getString("name"),
                        rs.getString("number"), rs.getString("git"), rs.getString("blog"), rs.getDate("create_date"), rs.getTime("create_date"));
                DBUtil.extracted(rs, ps, con);
                return addTemp;
            }
            DBUtil.extracted(rs, ps, con);
            throw new GBException("信息不存在");
        } catch (SQLException ex) {
            Logger.getLogger(AddTempDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    @Override
    public List<AddTemp> list() {
        try {
            List<AddTemp> list = new ArrayList<AddTemp>();
            con = DBUtil.getConnection();
            String sql = "select * from add_temp;";
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                AddTemp addTemp = new AddTemp(rs.getInt("id"), rs.getString("name"),
                        rs.getString("number"), rs.getString("git"), rs.getString("blog"), rs.getDate("create_date"), rs.getTime("create_date"));
                list.add(addTemp);
            }
            DBUtil.extracted(rs, ps, con);
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(AddTempDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    @Override
    public List<AddTemp> list(Pager page) {
        try {
            List<AddTemp> list = new ArrayList<AddTemp>();
            con = DBUtil.getConnection();
            String sql = "select * from add_temp;";
            ps = con.prepareStatement(sql);
            ps.setMaxRows(page.getEndIndex());
            rs = ps.executeQuery();
            if(page.getBeginIndex() > 0){
                rs.absolute(page.getBeginIndex());
            }
            while (rs.next()) {
                AddTemp addTemp = new AddTemp(rs.getInt("id"), rs.getString("name"),
                        rs.getString("number"), rs.getString("git"), rs.getString("blog"), rs.getDate("create_date"), rs.getTime("create_date"));
                list.add(addTemp);
            }
            DBUtil.extracted(rs, ps, con);
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(AddTempDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public int count() {
        int count = 0;
        try {
            con = DBUtil.getConnection();
            String sql = "select count(*) from add_temp;";
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            if (rs.next()) {
                count = rs.getInt("count(*)");
            }
            DBUtil.extracted(rs, ps, con);
            return count;
        } catch (SQLException ex) {
            Logger.getLogger(AddTempDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
}
