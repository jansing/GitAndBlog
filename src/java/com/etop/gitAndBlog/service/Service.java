/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etop.gitAndBlog.service;

import com.etop.gitAndBlog.common.Pager;
import com.etop.gitAndBlog.dao.DAOFactory;
import com.etop.gitAndBlog.dao.IMessageDao;
import com.etop.gitAndBlog.model.AddTemp;
import com.etop.gitAndBlog.model.Message;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class Service {
    public static List<Message> getListByNumber(String number){
        return DAOFactory.getMessageDao().listByNumber(number);
    }
    public static List<List<Message>> getListByNumber(){
        IMessageDao messageDao = DAOFactory.getMessageDao();
        List<List<Message>> lists = new ArrayList<List<Message>>();
        lists.add(messageDao.listByNumber("2012"));
        lists.add(messageDao.listByNumber("2013"));
        lists.add(messageDao.listByNumber("2014"));
        return lists;
    }
    public static void addAddTemp(AddTemp addTemp){
        DAOFactory.getAddTemoDao().add(addTemp);
    }
    public static List<AddTemp> getAddTempList(){
        return DAOFactory.getAddTemoDao().list();
    }
    public static List<AddTemp> getAddTempList(Pager page){
        return DAOFactory.getAddTemoDao().list(page);
    }
}
