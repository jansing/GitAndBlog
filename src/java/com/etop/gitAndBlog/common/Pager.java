/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etop.gitAndBlog.common;

import java.util.List;

/**
 * 分页
 *
 * @author Administrator
 */
public class Pager {

    private int totalPage;     //总页数  
    private int totalCount;    //总记录数  
    private int currentPage;   //当前页  
    private int pageSize;      //每页的数量  
    private int beginIndex;     //当前页第一条索引
    private int endIndex;       //当前页最后索引
    
    public static final int defaultPageSize = 9;

    public Pager() {
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getBeginIndex() {
        return beginIndex;
    }

    public void setBeginIndex(int beginIndex) {
        this.beginIndex = beginIndex;
    }

    public int getEndIndex() {
        return endIndex;
    }

    public void setEndIndex(int endIndex) {
        this.endIndex = endIndex;
    }

    /**
     * 分页处理
     *
     * @param currentPage 当前页
     * @param pageSize 每页的数量
     * @param totalCount 总记录数
     */
    public void paging(int currentPage, int pageSize, int totalCount) {
        this.currentPage = currentPage;
        this.pageSize = pageSize;
        this.totalCount = totalCount;
        this.totalPage = (totalCount + pageSize -1) / pageSize;

        if (currentPage < 1) {
            this.currentPage = 1;
        } else if(currentPage > totalPage){
            this.currentPage = totalPage;
        }
        
        this.beginIndex = (currentPage - 1) * pageSize;
        this.endIndex = beginIndex + pageSize - 1;
    }
}
