/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etop.gitAndBlog.servlet;

import com.etop.gitAndBlog.common.Pager;
import com.etop.gitAndBlog.dao.DAOFactory;
import com.etop.gitAndBlog.model.AddTemp;
import com.etop.gitAndBlog.model.Message;
import com.etop.gitAndBlog.service.Service;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.omg.DynamicAny.DynAnyFactory;

/**
 *
 * @author Administrator
 */
public class OperationServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");       
        String operation = request.getParameter("operation");
        if(operation == null){
            toMain(request, response);
        } else{
            switch(operation){
                case "addTemp":
                    toAddTemp(request, response);
                    break;
            }
        }
    }

    public void addAddTemp(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        AddTemp addTemp = new AddTemp();
        addTemp.setName(request.getParameter("personName"));
        addTemp.setNumber(request.getParameter("idNumber"));
        addTemp.setGit(request.getParameter("git"));
        addTemp.setBlog(request.getParameter("blog"));
        Service.addAddTemp(addTemp);
        toAddTemp(request, response);
    }
    
    public void toAddTemp(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        List<AddTemp> list = Service.getAddTempList();
        request.setAttribute("addTempList", list);
        request.getRequestDispatcher("/WEB-INF/gitAndBlogInput.jsp").forward(request, response);
    }
    
    public void toMain(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        request.setAttribute("lists", Service.getListByNumber());
        request.getRequestDispatcher("/WEB-INF/gitAndBlog.jsp").forward(request, response);
        return;
    }
    
    
    
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        String operation = request.getParameter("operation");
            switch(operation){
                case "addAddTemp":
                    addAddTemp(request, response);
                    break;
                default :
                    toMain(request, response);
                    break;
            }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
