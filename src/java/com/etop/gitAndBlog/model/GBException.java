/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etop.gitAndBlog.model;

/**
 *
 * @author Administrator
 */
public class GBException extends RuntimeException{
    public GBException(){
    }
    public GBException(String message){
        super(message);
    }
    public GBException(String message, Throwable cause){
        super(message, cause);
    }
    public GBException(Throwable cause){
        super(cause);
    }
    public GBException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace){
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
