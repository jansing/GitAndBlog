/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etop.gitAndBlog.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Administrator
 */
public class DBUtil {
    
    public static Connection getConnection(){
        try {
            Connection con = null;
            String url = "jdbc:mysql://localhost:3306/gitandblog";
            String username = "root";
            String password = "lzjansing";
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(url, username, password);
            return con;
        } catch (SQLException ex) {
            Logger.getLogger(DBUtil.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DBUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static void extracted(ResultSet rs, PreparedStatement ps, Connection con){
        try {
            rsClose(rs);
            psClose(ps);
            conClose(con);
        } catch (SQLException ex) {
            Logger.getLogger(DBUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void rsClose(ResultSet rs) throws SQLException{
        if(rs!=null){
            rs.close();
        }
    }
    
    public static void psClose(PreparedStatement ps) throws SQLException{
        if(ps!=null){
            ps.close();
        }
    }
    
    public static void conClose(Connection con) throws SQLException{
        if(con!=null){
            con.close();
        }
    }
    
}
