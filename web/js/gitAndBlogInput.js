$(function(){
	$(".gitAndBlogInputPanel #submit").click(function(){
		if(isIdNumberError($(".gitAndBlogInputPanel #idNumber").val())){
			$(".gitAndBlogInputPanel .tip").slideDown();
			return false;
		}else{
			return true;
		}
	});
	$(".gitAndBlogInputPanel #idNumber").focus(function(){
		$(".gitAndBlogInputPanel .tip").slideUp();
	});
	
	$(".gitAndBlogInput .forScroll").mCustomScrollbar({
		theme:"white",
		scrollButtons:{
			enable:false
		},
		autoHideScrollbar:true,
		scrollInertia:100,
		horizontalScroll:false,
	});	
	
	setTdHoverListener();
	addModifyListenerInGitAndBlog();
})

function isIdNumberError(value){
	if(value.length < 12 || value.length > 12){
		return "idIsTooShortOrTooLong";
	}
	if(isNaN(value)){
		return "notANumber";
	}
	if(!(value.substring(0, 2)=="20")){
		return "notStandard";
	}
	return false;
};

function setTdHoverListener(){
	$(".submitSuccessful td a").hover(function(){
		
		$(".gitAndBlogInput .overflowTip")
			.html($(this).html());
		
		if($(this).html().length>24){

			$(".gitAndBlogInput .overflowTip").stop(true, true);

			var topDis = Math.round($(this).offset().top);
			var leftDis = Math.round($(this).offset().left);
			var tdWidth = $(this).width();
			var spanWidth = $(".gitAndBlogInput .overflowTip").width();

			var shouldTop = topDis - 45;
			var finalTop = topDis - 35;
			var shouldLeft = leftDis+tdWidth/2-spanWidth/2-80;

			$(".gitAndBlogInput .overflowTip")
				.css({
					left:shouldLeft+"px", 
					top:shouldTop+"px", 
					opacity:"0",
					display:"block"});
			$(".gitAndBlogInput .overflowTip")
				.animate({
					opacity:"1", 
					top:finalTop+"px"
				}, 200);
		}
	},function(){
		$(".gitAndBlogInput .overflowTip").css({display:"none"});
	});
}

function addModifyListenerInGitAndBlog(){
	$(".gitAndBlogInput .modifyInf input[type='text']")[0].focus();
	$(".gitAndBlogInput .submitSuccessful table button")
	.click(function(){
		$(".gitAndBlogInput .mask").fadeIn("fast");
		$(".gitAndBlogInput .modifyInf").fadeIn("fast");
		
		$("#MDFpersonName").val($(".gitAndBlogInput .SuccName").html());
		$("#MDFidNumber").val($(".gitAndBlogInput .SuccNumber").html());
		$("#MDFgit").val($(".gitAndBlogInput .SuccGit").find("a").html());
		$("#MDFblog").val($(".gitAndBlogInput .SuccBlog").find("a").html());
		$("#hidden").val($(this).parent().parent().find(".SuccNumber").html());
		alert($(this).parent().parent().find(".SuccNumber").html());
	})
}