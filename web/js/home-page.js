/*bug
 * 
 * ie11中签到按钮移动错误（载入时与单击时）
 * 
 * 
 * 
 * 
 * */

var $hour;//时间 小时
var $minute;//时间 分钟
var $second;//时间 秒
var $history;
var $menuDepA;
var $menuHoverLi;
var $nthChildSev;
var $nthChildSix;
var $nthChildFiv;
var $nthChildFor;
var $nthChildThr;
var $nthChildSec;
var $nthChildFir;
var $currentAndExit;
var $check;
var $allPage;
var $scrollContent;
var $subpage;

$(function(){
	
	$time = $(".all-page .time");
	$timeSpan = $(".all-page .time span");
	$history = $(".home-page .history");
	$menuDepA = $(".parent>li>ul>li>a");
	$menuHoverLi = $(".parent>li");
	$hour = $(".hour");
	$minute = $(".minute");
	$second = $(".second");
	$nthChildSev = $(".home-page .parent>li:nth-child(7)");
	$nthChildSix = $(".home-page .parent>li:nth-child(6)");
	$nthChildFiv = $(".home-page .parent>li:nth-child(5)");
	$nthChildFor = $(".home-page .parent>li:nth-child(4)");
	$nthChildThr = $(".home-page .parent>li:nth-child(3)");
	$nthChildSec = $(".home-page .parent>li:nth-child(2)");
	$nthChildFir = $(".home-page .parent>li:nth-child(1)");
	$currentAndExit = $(".home-page .current-and-exit");
	$check = $(".home-page .check");	
	$allPage = $(".all-page");
	$scrollContent = $(".home-page .content");
	$subpage = $(".all-page .parent ul a");
	
//	firstMove();
	
	hideHistory();
	
//	window.onload = function(){
//		secondMove();
//	};
	
	addCheckListener();
	
	addNavigationListener();
	
	setTime();
	
	setWidth();
	
	setScrollStyle();
});

function addNavigationListener(){
	//选项
	$menuDepA.slideUp(0);
	$menuHoverLi.hover(function(){
		$(this).find("ul>li>a").slideDown(100);
//		$(this).find(">a").css({
//			"background-color":"white"
//		});
	},
	function(){
		$(this).find("ul>li>a").slideUp(100);
//		$(this).find(">a").css({
//			"background-color":""
//		});
	});
};

function setTime(){

	var today=new Date();
	var h=today.getHours();
	var m=today.getMinutes();
	var s=today.getSeconds();
	
	h = checkTime(h);
	m = checkTime(m);
	s = checkTime(s);
	
	$hour.html(h);//$hour[0].innerHTML = h; 也可以用这种方法
	$minute.html(m);
	$second.html(s);
	
	setTimeout(setTime, 500);
}

function checkTime(check){
	
	if(check < 10){//check.length == 1不行
		return "0" + check;
	}else{
		return check;
	}
}

function addCheckListener(){
	$check.click(function(){
		
		$(this).html("已签到");
//		
//		$(this).velocity({
//			fontSize:"18px",
//			left:"87%", 
//			width:"80px", 
//			height:"80px", 
//			borderRadius:"50%"
//		}, 300, "easeOut");
//		
//		$(this).addClass("checked");
//		$(this).blur();
//		showHistory();
	});
}
function firstMove(){
//	$nthChildSev.animate({left:"-160px"}, 0);
//	$nthChildSix.animate({left:"-160px"}, 0);
//	$nthChildFiv.animate({left:"-160px"}, 0);
//	$nthChildFor.animate({left:"-160px"}, 0);
//	$nthChildThr.animate({left:"-160px"}, 0);
//	$nthChildSec.animate({left:"-160px"}, 0);
//	$nthChildFir.animate({left:"-160px"}, 0);
//	$currentAndExit.animate({top:"-200px"}, 0);
	$check.velocity({top:"120%"}, 0);
	
};

function secondMove(){
//	$nthChildSev.animate({left:"630px"}, 1790, "easeOutBack");
//	$nthChildSix.animate({left:"525px"}, 1700, "easeOutBack");
//	$nthChildFiv.animate({left:"420px"}, 1500, "easeOutBack");
//	$nthChildFor.animate({left:"315px"}, 1500, "easeOutBack");
//	$nthChildThr.animate({left:"210px"}, 1300, "easeOutBack");
//	$nthChildSec.animate({left:"105px"}, 1200, "easeOutBack");
//	$nthChildFir.animate({left:"0"}, 1100, "easeOutBack");
//	$currentAndExit.animate({top:"0"}, 1500, "easeOutBack");
	$check.velocity({top:"48%"}, 600, "easeOut");
	$check.velocity({top:"50%"}, 370, "easeOut");
}
function hideHistory(){
	$history.slideUp(0);
}

function showHistory(){
	$history.slideDown("fast");
}


function setWidth(){
	
	updateWidth();
	
	$(window).resize(function(){//窗口改变大小的时候
		updateWidth();
	});

};

function updateWidth(){
	if($(window).width() > 1680){
		$allPage.css({
			"background-size":"100%"
		});
		
	}else{
		$allPage.css({
			"background-size":""
		});
	}	
};

function setScrollStyle(){
//	$scrollContent.mCustomScrollbar({
//		theme:"white",
//		scrollButtons:{
//			enable:false
//		},
//		autoHideScrollbar:true,
//		scrollInertia:100,
//		horizontalScroll:false,
//	});
}
