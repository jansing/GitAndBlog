$(function(){
	
	$(".borrowInformation .borrowPanel .scrollContent input").click(function(){
		var $thisInput = $(this).parent();
		$(".borrowInformation .conformTip").css({display:"block"});
		$(".borrowInformation .mask").css({display:"block"});
		$(".borrowInformation .conformTip .cancel").click(function(){
			$(".borrowInformation .mask").css({display:"none"});
			$(".borrowInformation .conformTip").css({display:"none"});
		});
		$(".borrowInformation .mask").click(function(){
			$(".borrowInformation .mask").css({display:"none"});
			$(".borrowInformation .conformTip").css({display:"none"});			
		});
		$(".borrowInformation .conformTip .yes").click(function(){
			$thisInput.css({background:"red"});
			$thisInput.submit();
		});		
		return false;
	});
	
	$(".borrowInformation .borrowPanel .scrollContent").mCustomScrollbar({
		theme:"white",
		scrollButtons:{
			enable:false
		},
		autoHideScrollbar:true,
		scrollInertia:100,
		horizontalScroll:false,
	});
});

