var $hour;//时间 小时
var $minute;//时间 分钟
var $second;//时间 秒
var $randomArray;//背景图片随机
var $Error;//

$(function(){
	
//	addUl();
	
	setStyleByBrowser();
	
//	setPictureOrderByRandom();
	
//	setBodyBackground();//防止在ie8及以下浏览器中，看不到轮播的背景
	
	setWidth();
	
	setFocus();
	
	formListener();
	
	setTime();
});

function setTime(){
	
	$hour = $(".hour");
	$minute = $(".minute");
	$second = $(".second");

	var today=new Date();
	var h=today.getHours();
	var m=today.getMinutes();
	var s=today.getSeconds();
	
	h = checkTime(h);
	m = checkTime(m);
	s = checkTime(s);
	
	$hour.html(h);//$hour[0].innerHTML = h; 也可以用这种方法
	$minute.html(m);
	$second.html(s);
	
	setTimeout(setTime, 500);
}

function checkTime(check){
	
	if(check < 10){//check.length == 1不行
		return "0" + check;
	}else{
		return check;
	}
}

function setFocus(){
	$("[type=text]")[0].focus();
	
};

function setWidth(){
	
	updateWidth();
	
	$(window).resize(function(){//窗口改变大小的时候
		updateWidth();
	});

};

function updateWidth(){
	if($(window).width() > 1680){
		$("body").css({
			"background-size":"100%"
		});
		
	}else{
		$("body").css({
			"background-size":""
		});
	}	
};

function setPictureOrderByRandom(){
	$randomArray = new Array(4);
	$randomArray[0] = "fir";
	$randomArray[1] = "sec";
	$randomArray[2] = "thr";
	$randomArray[3] = "fth";
	
	for(var i = 0; i < 4; i++){
		var $randomNumber = Math.floor(Math.random() * 4);
		var temp = $randomArray[i];
		$randomArray[i] = $randomArray[$randomNumber];
		$randomArray[$randomNumber] = temp;
	}
	
	$(".background :nth-child(1)").removeClass().addClass($randomArray[0]);
	$(".background :nth-child(2)").removeClass().addClass($randomArray[1]);
	$(".background :nth-child(3)").removeClass().addClass($randomArray[2]);
	$(".background :nth-child(4)").removeClass().addClass($randomArray[3]);
}

//function setBodyBackground(){
//	switch($randomArray[0]){
//		case "fir":{
////			alert("1");
//			$("body").addClass("mode1");
//			break;
//		}
//		case "sec":{
////			alert("2");
//			$("body").addClass("mode2");
//			break
//		}
//		case "thr":{
////			alert("3");
//			$("body").addClass("mode3");
//			break
//		}
//		case "fth":{
////			alert("4");
//			$("body").addClass("mode4");
//			break;
//		}
//	}
//}

//function addUl(){
//	/*必须写成一行，否则报错*/$(".content").before("<ul class='background'><li>background</li><li>background</li><li>background</li><li>background</li></ul>");
//};

function formListener(){
	$("[type=text]").parent().append("<span class='usernameError'>请输入正确的学号</span>");
	$(".usernameError").hide();
	$("[type=password]").parent().append("<span class='passwordError'>请输入正确的密码</span>");
	$(".passwordError").hide();
	$("[type=text]").blur(function(){
		var errors = isUsernameError(this.value);
		if(errors){
			$(".usernameError").show("fast");
		}
	});
	
	$("[type=text]").focus(function(){
		$(".usernameError").hide("fast");
	});
	
	$("[type=password]").blur(function(){
		var errors = isPassWordError(this.value);
		if(errors){
			$(".passwordError").show("fast");		
		}
	});
	
	$("[type=password]").focus(function(){
		$(".passwordError").hide("fast");
	});
	
	$("[type=submit]").click(function(){
		$("[type=text]").trigger("blur");
		$("[type=password]").trigger("blur");
		if(!$(".usernameError").is(":hidden") || !$(".passwordError").is(":hidden")){
			return false;
		}
		window.location.href="home-page.html";
		return false;//来自百度：虽然跳转代码是在return true前面，看起来应该是先执行跳转，但是浏览器都不是这样实现的，所以只能return false阻止掉浏览器的默认动作。
	});
};

function isUsernameError(value){
	if(value.length < 12 || value.length > 12){
		return "idIsTooShortOrTooLong";
	}
	if(isNaN(value)){
		return "notANumber";
	}
	if(!(value.substring(0, 2)=="20")){
		return "notStandard";
	}
	return false;
};

function isPassWordError(value){
	if(value.length == 0){
		return "empty";
	}
	
	return false;
}

function setStyleByBrowser(){
	var explorer = window.navigator.userAgent ; 
	if(explorer.indexOf("Chrome") >= 0){
		$(".background li").css({
			"opacity":"1"
		});
	}
}
