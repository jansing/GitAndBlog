$(function(){
	setBorrowScroll();
})

function setBorrowScroll(){
	$(".attendanceHistory .history .content").mCustomScrollbar({
		theme:"white",
		scrollButtons:{
			enable:false
		},
		autoHideScrollbar:true,
		scrollInertia:100,
		horizontalScroll:false,
	});	
}
