var $gitAndBlogContent;
var $gitAndBlogUl;
var $gitAndBlogPar;

$(function(){	
	$gitAndBlogContent = $(".gitAndBlog .gitPanel .content");
	$gitAndBlogPar = $(".gitAndBlog .gitPanel .par .available");
	$gitAndBlogUl = $(".gitAndBlog .gitPanel .par>li>ul");
	
	setGitAndBlogContent();
	addHoverListenerTogitAndBlogPar();
})

function setGitAndBlogContent(){
	$gitAndBlogContent.mCustomScrollbar({
		theme:"white",
		scrollButtons:{
			enable:false
		},
		autoHideScrollbar:true,
		scrollInertia:100,
		horizontalScroll:false,
	});
};

function addHoverListenerTogitAndBlogPar(){
	$gitAndBlogPar.hover(function(){
		$(this).find("ul").css({opacity:"0", display:"block", top:"-55px"}, 0);
		$(this).find("ul").animate({opacity:"1", top:"-38px"}, 200);
//		alert("yes");
	}, function(){
		$(this).find("ul").fadeOut(200);
	})
};
