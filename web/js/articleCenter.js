var $articleContent;
var $search;
var $tableFirstColumn;
var $tableOfEachRow;
var $borrowAndInformationButton;
var $borrowButton;
var $informationButton;
var $isBorrow = false;
var $mask;
var $returnMask;
var $cancelButton;
var $confirmButton;
var $borrowPanel;
var $returnPanel;
var $returnTitle;
var $returnDayInput;
var $informationConfirmbutton;
var $whichButton;

$(function(){
	$articleContent = $(".articleCenter .articleDisplay .content");
	$search = $(".articleCenter #search");
	$tableOfEachRow = $(".articleCenter table tr:not(:first)");
	$tableFirstColumn = $tableOfEachRow.find("td:first");
	$borrowAndInformationButton = $(".articleCenter table button");
	$borrowButton = $(".articleCenter table button:contains('点我借阅')");
	$informationButton = $(".articleCenter table button:contains('借出')");
	$mask = $(".articleCenter .mask");
	$returnMask = $(".articleCenter .returnMask");
	$cancelButton = $(".articleCenter .mask input[type=button]");
	$confirmButton = $(".articleCenter .mask input[type=submit]")
	$borrowPanel = $(".articleCenter .mask .borrow");
	$returnPanel = $(".article .returnMask .return");
	$returnTitle = $(".articleCenter .return .title");
	$returnDayInput = $(".articleCenter .borrow .returnDay");
	$informationConfirmbutton = $(".articleCenter .returnMask input[type=submit]");
//	$tableFirstColumn.each(function(){
//		alert($(this).html());
//	});	
	setArticleDisplayScroll();
	
	setSearchFocusListener();
	
	addBorrowButton();
	
	setBorrowButtonListener();
});

function setArticleDisplayScroll(){
	$articleContent.mCustomScrollbar({
		theme:"white",
		scrollButtons:{
			enable:false
		},
		autoHideScrollbar:true,
		scrollInertia:100,
		horizontalScroll:false,
	});
}

function setSearchFocusListener(){
	$search.focus(function(){
		$search.velocity({width:"180px"}, 200, "ease");
		addSearchListener();
	});
	$search.blur(function(){
		$search.velocity({width:"100px"}, 200, "ease")
	});
}

function addSearchListener(){
	$search.on('input', function(){
		for(var i = 0; i < $tableFirstColumn.length; i++){
			var $oneTd = $tableFirstColumn.get(i).innerHTML.toLowerCase();
			while($oneTd.indexOf(" ") != -1){
				$oneTd = $oneTd.replace(" ", "");
			}
			if($oneTd.search($search.val().toLowerCase()) != -1){
				$tableOfEachRow.get(i).style.display = "block";
			}else{
				$tableOfEachRow.get(i).style.display = "none";
			}
			var $array = $search.val().toLowerCase().split(" ");
			for(var j = 0; j < $array.length; j++){
				if($oneTd.search($array[j]) != -1){
					if($array[j] != ""){
						$tableOfEachRow.get(i).style.display = "block";	
					}
				}				
			}
		}
	});
//	(function(event){//这种做法输入中文的时无反应
//		setTimeout(function(){
//			//当用户按下按键的时候，JavaScript 引擎需要执行 keydown 的事件处理程序，
//			//然后更新文本框的 value 值，这两件事也需要按顺序来，事件处理程序执行时，更
//			//新 value 值的任务则进入队列等待。所以我们在 keydown 的事件处理程序里是无
//			//法得到更新后的 value 的，利用 setTimeout，我们把取 value 的操作放入队
//			//列，放在更新 value 值以后，这样便达到了目的。
//			$("title").html($search.val());
//	}, 0);
};

function addBorrowButton(){
	$borrowAndInformationButton.hover(function(){
		if($(this).html() == "借出"){
			$isBorrow = true;
			$(this).html("归还");
		}else{
			$isBorrow = false;
			$(this).html("借阅");
		}
	},function(){
		if($isBorrow){
			$(this).html("借出");
		}else{
			$(this).html("点我借阅")
		}
	});
};

function setBorrowButtonListener(){
	
	$borrowButton.click(function(){
		$mask.css({opacity:"0", display:"block"});
		$mask.velocity({opacity:"1"}, 200);
		$returnDayInput.focus();
		$whichButton = $(this);
	});
	$confirmButton.click(function(){
		var re = /\d{4}年\d{1,2}月\d{1,2}日/;
		var value = $returnDayInput.val();
		var isValid = re.exec(value);
		if(isValid != null){
			$whichButton.html("借出");
			$whichButton.parent().parent().find("td:nth-child(3)").html($(".articleCenter .current-user span").html());
			$mask.velocity({opacity:"0"}, 200, function(){
				$mask.css({display:"none"});
			});
			//移除事件
			$borrowButton.unbind("click");
			$informationButton.unbind("click");
			//重新设置可借阅按钮
			$borrowButton = $(".articleCenter table button:contains('点我借阅')");
			$informationButton = $(".articleCenter table button:contains('借出')");
			//重新绑定事件
			$borrowButton.click(function(){
				$mask.css({opacity:"0", display:"block"});
				$mask.velocity({opacity:"1"}, 200);
				$returnDayInput.focus();
				$whichButton = $(this);
			});
			$informationButton.click(function(){
				$returnMask.css({opacity:"0", display:"block"});
				$returnMask.velocity({opacity:"1"}, 0,function(){
					$returnTitle.html($(this).parent().find("td:first").html());
					$returnMask.css({opacity:"0"});
					$returnMask.velocity({opacity:"1"}, 200);//如果不这样写，会出现如下状况：panel出来，标题暂停一会才换。试过其它解决方法，只有这个起作用。
				});
			});
		}		
	});
	$cancelButton.click(function(){
		$mask.velocity({opacity:"0"}, 200, function(){
			$mask.css({display:"none"});
		});
	});
	$mask.click(function(){
		$mask.velocity({opacity:"0"}, 200, function(){
			$mask.css({display:"none"});
		});
	});
	$borrowPanel.click(function(){
		return false;
	});
	$informationButton.click(function(){
		$returnMask.css({opacity:"0", display:"block"});
		$returnMask.velocity({opacity:"1"}, 0,function(){
			$returnTitle.html($(this).parent().find("td:first").html());
			$returnMask.css({opacity:"0"});
			$returnMask.velocity({opacity:"1"}, 200);//如果不这样写，会出现如下状况：panel出来，标题暂停一会才换。试过其它解决方法，只有这个起作用。
		});
	});
	$informationConfirmbutton.click(function(){
		$returnMask.velocity({opacity:"0"}, 200, function(){
			$returnMask.css({display:"none"});
		});
	});
	$returnMask.click(function(){
		$returnMask.velocity({opacity:"0"}, 200, function(){
			$returnMask.css({display:"none"});
		});
	});
}
