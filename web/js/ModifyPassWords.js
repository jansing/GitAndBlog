var $modPasWoPnl;
var $submitButton;
var $newPasswords;
var $confirmPasswords;
var $tip;

$(function(){
	
	$(".mod-pasWo-pge form").parent().append("<span class='tip'>前后输入的密码不相同！</span>");
	$tip = $(".mod-pasWo-pge .mod-pasWo-pnl .tip");
	$submitButton = $(".mod-pasWo-pge .mod-pasWo-pnl .submit");
	$newPasswords = $(".mod-pasWo-pge #new-passwords");
	$confirmPasswords = $(".mod-pasWo-pge #confirm-passwords");
	
	$tip.slideUp(0);
	
	addFormListener();
});

function addFormListener(){
	$submitButton.click(function(){
		if($newPasswords.val() != $confirmPasswords.val()){
			$tip.slideDown(100);			
			return false;		
		}
	});
	$confirmPasswords.focus(function(){
		$(".mod-pasWo-pge .tip").slideUp("fast");
	});
	$newPasswords.focus(function(){
		$(".mod-pasWo-pge .tip").slideUp("fast");
	});
}
