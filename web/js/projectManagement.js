$(function(){
	
	detailMoveOut();
	
	addHeadLineClickListener();
	
	setScroll();
});

function detailMoveOut(){
	$(".projectManagement .detail").slideUp(0);
	$(".projectManagement .firstChild").slideDown(0);
}

function addHeadLineClickListener(){
	$(".projectManagement .singlePanel .headline").click(function(){
		$(this).parent().find(".detail").slideToggle(200);
	});
}

function setScroll(){
	$(".projectManagement .forScroll").mCustomScrollbar({
		theme:"white",
		scrollButtons:{
			enable:false
		},
		autoHideScrollbar:true,
		scrollInertia:100,
		horizontalScroll:false,
	});
};
