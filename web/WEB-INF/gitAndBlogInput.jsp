<%-- 
    Document   : gitAndBlogInput
    Created on : 2015-4-11, 10:01:42
    Author     : Administrator
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/home-page.css"/>
		<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/gitAndBlogInput.css"/>
		<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/jquery.mCustomScrollbar.css"/>
		<script src="<%=request.getContextPath()%>/js/jquery-1.11.2.js"></script>
		<script src="<%=request.getContextPath()%>/js/jquery.mCustomScrollbar.concat.min.js"></script>
		<script src="<%=request.getContextPath()%>/js/home-page.js"></script>
		<script src="<%=request.getContextPath()%>/js/gitAndBlogInput.js"></script>
		<title>交流信息录入</title>
    </head>
	<body class="all-page gitAndBlogInput">
		<div class="header">
			<ul class="parent">
				<li><a href="home-page.html">首页</a></li>
				<li><a href="#">个人信息</a>
					<ul>
						<li><a href="modify.html">修改个人信息</a></li>
						<li><a href="modifyPassWords.html">修改个人密码</a></li>
						<li><a href="borrowInformation.html">查看借阅信息</a></li>
						<li><a href="attendanceHistory.html">查看签到历史</a></li>
					</ul>
				</li>
				<li><a href="#">文件中心</a>
					<ul>
						<li><a href="#">上传文件</a></li>
						<li><a href="#">查找文件</a></li>
					</ul>
				</li>
				<li><a href="articleCenter.html">物品中心</a>
					<ul>
					</ul>
				</li>
				<li><a href="#">项目管理</a>
					<ul>
						<li><a href="projectManagement.html">所有项目</a></li>
						<li><a href="#">我的项目</a></li>
					</ul>
				</li>
				<li><a href="#">活动报名</a>
					<ul>
						<li><a href="#">查看可报名的活动</a></li>
						<li><a href="#">查看已报名的活动</a></li>
					</ul>
				</li>
				<li><a href="#">分享交流</a>
					<ul></ul>
				</li>			
			</ul>
			<div class="current-and-exit">
				<a class="exit" href="login2th.html">退出登录</a>
				<p class="current-user">当前用户：赖智辉</p>
			</div>
		</div>
		<p class="copyrights">copyrights © 2015-2016 SCAU E-TOP</p>
		<div class="gitAndBlogInputPanel">
			<p class="title">请输入你的相关信息</p>
			<form action="<%=request.getContextPath()%>/OperationServlet?operation=addAddTemp"method="post">
				<label for="personName">姓名</label>
				<input id="personName" name="personName" type="text" />
				
				<label for="idNumber">学号</label>
				<input id="idNumber" name="idNumber" type="text" />
				
				<label for="git">git</label>
				<input id="git" name="git" type="text" />
				
				<label for="blog">blog</label>
				<input id="blog" name="blog" type="text" />
				<a href="<%=request.getContextPath()%>/OperationServlet" class="cancelButton">返回</a>
				<input id="submit" type="submit" value="确认"/>
			</form>
			<p class="tip">请输入正确的学号</p>
		</div>

		<div class="submitSuccessful">
			<p class="title">已提交</p>
			<div class="forScroll">
				<table>
                    <tr><th>姓名</th><th>学号</th><th>git</th><th>blog</th><th>修改</th></tr>
                    <c:forEach items="${addTempList}" var="addTemp">
                        <tr><span class="hiddenId">${addTemp.id}</span><td class="SuccName">${addTemp.name}</td><td class="SuccNumber">${addTemp.number}</td><td class="SuccGit"><a href="${addTemp.git}">${addTemp.git}</a></td><td class="SuccBlog"><a href="${addTemp.blog}">${addTemp.blog}</a></td><td><button>修改</button></td></tr>
                    </c:forEach>
				</table>
			</div>
		</div>
		
		<form class="modifyInf" action="" method="">
			<label for="MDFpersonName">姓名</label>
			<input id="MDFpersonName" name="MDFpersonName" type="text" value=""/>
			
			<label for="MDFidNumber">学号</label>
			<input id="MDFidNumber" name="MDFidNumber" type="text" value=""/>
			
			<label for="MDFgit">git</label>
			<input id="MDFgit" name="MDFgit" type="text" value=""/>
			
			<label for="MDFblog">blog</label>
			<input id="MDFblog" name="MDFblog" type="text" value=""/>
			<button class="MDFcancel">返回</button>
			<input id="MDFsubmit" type="submit" value="确认"/>
			<input id="hidden" type="text" value=""/>
		</form>
		<div class="mask"></div>
		<span class="overflowTip"><span class="arrow"></span></span>
	</body>
</html>
