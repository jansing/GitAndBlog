<%-- 
    Document   : gitAndBlog
    Created on : 2015-4-10, 21:02:40
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/home-page.css"/>
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/gitAndBlog.css"/>
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/jquery.mCustomScrollbar.css"/>
        <script src="<%=request.getContextPath()%>/js/jquery-1.11.2.js"></script>
        <script src="<%=request.getContextPath()%>/js/jquery.easing.1.3.js"></script>
        <script src="<%=request.getContextPath()%>/js/Velocity.js"></script>
        <script src="<%=request.getContextPath()%>/js/jquery.mCustomScrollbar.concat.min.js"></script>
        <script src="<%=request.getContextPath()%>/js/home-page.js"></script>
        <script src="<%=request.getContextPath()%>/js/gitAndBlog.js"></script>
        <title>交流分享</title>
    </head>
    <body class="all-page gitAndBlog">
        <div class="header">
            <ul class="parent">
                <li><a href="#">首页</a></li>
                <li><a href="#">个人信息</a>
                    <ul>
                        <li><a href="#">修改个人信息</a></li>
                        <li><a href="#">修改个人密码</a></li>
                        <li><a href="#">查看借阅信息</a></li>
                        <li><a href="#">查看签到历史</a></li>
                    </ul>
                </li>
                <li><a href="#">文件中心</a>
                    <ul>
                        <li><a href="#">上传文件</a></li>
                        <li><a href="#">查找文件</a></li>
                    </ul>
                </li>
                <li><a href="#">物品中心</a>
                    <ul>
                    </ul>
                </li>
                <li><a href="#">项目管理</a>
                    <ul>
                        <li><a href="#">所有项目</a></li>
                        <li><a href="#">我的项目</a></li>
                    </ul>
                </li>
                <li><a href="#">活动报名</a>
                    <ul>
                        <li><a href="#">查看可报名的活动</a></li>
                        <li><a href="#">查看已报名的活动</a></li>
                    </ul>
                </li>
                <li><a href="#">分享交流</a>
                    <ul></ul>
                </li>			
            </ul>
            <div class="current-and-exit">
                <a class="exit" href="#">退出登录</a>
                <p class="current-user">当前用户：赖智辉</p>
            </div>
        </div>
        <p class="copyrights">copyrights © 2015-2016 SCAU E-TOP</p>
        <div class="gitPanel">
            <p class="title">个人主页（git 博客）<a href="<%=request.getContextPath()%>/OperationServlet?operation=addTemp">添加个人信息</a></p>
            <div class="content">
                <c:forEach items="${lists}" var="list">
                <!--====================某一分类=======================-->
                <ul class="par">
                    <c:forEach items="${list}" var="message">
                        <li <c:if test="${message.git ne '#' or message.blog ne '#'}">class="available"</c:if>>${message.name}
                                <ul>
                                    <li>
                                    <c:choose>
                                        <c:when test="${message.git eq '#'}">
                                            <a class="invalid" href="#">git</a>
                                        </c:when>
                                        <c:otherwise>
                                            <a href="${message.git}" target="_Blank">git</a>
                                        </c:otherwise>
                                    </c:choose>
                                    <c:choose>
                                        <c:when test="${message.blog eq '#'}">
                                            <a class="invalid" href="#">blog</a>
                                        </c:when>
                                        <c:otherwise>
                                            <a href="${message.blog}"  target="_Blank">blog</a>
                                        </c:otherwise>
                                    </c:choose>
                                    <span></span>
                                </li>
                            </ul>
                        </li>
                    </c:forEach>
                </ul>
                </c:forEach>
            </div>
        </div>
    </body>
</html>
